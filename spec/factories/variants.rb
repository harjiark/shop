FactoryGirl.define do
  factory :variant do
    product nil
    sku "MyString"
    name "MyString"
    price "9.99"
    cost "9.99"
    deleted_at "2016-11-18 13:49:43"
    master false
    inventory nil
  end
end
