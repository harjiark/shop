FactoryGirl.define do
  factory :state do
    name "MyString"
    abbreviation "MyString"
    describe_as "MyString"
    country nil
    shipping_zone nil
  end
end
