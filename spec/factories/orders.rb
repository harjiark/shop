FactoryGirl.define do
  factory :order do
    number "MyString"
    email "MyString"
    state "MyString"
    user nil
    coupon nil
    active false
    shipped false
    shipments_counts 1
    calculated_at "2016-11-17 16:56:15"
    completed_at "2016-11-17 16:56:15"
    credited_amount "9.99"
  end
end
