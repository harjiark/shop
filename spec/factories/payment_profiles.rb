FactoryGirl.define do
  factory :payment_profile do
    user nil
    address nil
    payment_cim_id 1
    default false
    active false
    last_digits "MyString"
    month "MyString"
    year "MyString"
    cc_type "MyString"
    first_name "MyString"
    last_name "MyString"
    card_name "MyString"
  end
end
