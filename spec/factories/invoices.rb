FactoryGirl.define do
  factory :invoice do
    order ""
    amount "9.99"
    state "MyString"
    active false
    credited_amount ""
  end
end
