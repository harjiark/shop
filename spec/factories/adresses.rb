FactoryGirl.define do
  factory :adress do
    adress_type nil
    name "MyString"
    addresable_type "MyString"
    addresable_id 1
    address1 "MyString"
    address2 "MyString"
    city "MyString"
    state nil
    state_name "MyString"
    zip_code "MyString"
    alternative_phone "MyString"
    default false
    billing_default false
    active false
    country nil
  end
end
