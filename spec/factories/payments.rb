FactoryGirl.define do
  factory :payment do
    invoice nil
    confirmation_id 1
    amount "9.99"
    error "MyString"
    error_code "MyString"
    message "MyString"
    action "MyString"
    params "MyText"
    success false
    test false
  end
end
