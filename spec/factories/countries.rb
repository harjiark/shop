FactoryGirl.define do
  factory :country do
    name "MyString"
    abbreviation "MyString"
    shipping_zone nil
  end
end
