FactoryGirl.define do
  factory :cart_item do
    user nil
    cart nil
    variant nil
    quantity 1
    item_type nil
    active false
  end
end
