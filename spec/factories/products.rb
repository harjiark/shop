FactoryGirl.define do
  factory :product do
    name "MyString"
    descrtiption "MyText"
    product_keywords "MyText"
    shipping_category nil
    available_at "2016-11-18 10:06:08"
    deleted_at "2016-11-18 10:06:08"
    meta_keywords "MyString"
    meta_description "MyString"
    featured false
    brand nil
  end
end
