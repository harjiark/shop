FactoryGirl.define do
  factory :product_type do
    name "MyString"
    parent_id 1
    active false
    rgt 1
    lft 1
  end
end
