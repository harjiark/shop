FactoryGirl.define do
  factory :inventory do
    count_on_hand 1
    count_pending_to_customer 1
    count_pending_from_supplier 1
  end
end
