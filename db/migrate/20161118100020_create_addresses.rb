class CreateAddresses < ActiveRecord::Migration[5.0]
  def change
    create_table :addresses do |t|
      t.references :address_type, foreign_key: true
      t.string :name
      t.string :addresable_type
      t.integer :addresable_id
      t.string :address1
      t.string :address2
      t.string :city
      t.references :state, foreign_key: true
      t.string :state_name
      t.string :zip_code
      t.string :alternative_phone
      t.boolean :default
      t.boolean :billing_default
      t.boolean :active
      t.references :country, foreign_key: true

      t.timestamps
    end
  end
end
