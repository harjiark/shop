class CreateCoupons < ActiveRecord::Migration[5.0]
  def change
    create_table :coupons do |t|
      t.string :type
      t.string :code
      t.decimal :amount
      t.decimal :minimum_value
      t.integer :percent
      t.text :description
      t.datetime :starts_at
      t.datetime :expires_at

      t.timestamps
    end
    add_index :coupons, :code, unique: true
  end
end
