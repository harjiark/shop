class CreateCartItems < ActiveRecord::Migration[5.0]
  def change
    create_table :cart_items do |t|
      t.references :user, foreign_key: true
      t.references :cart, foreign_key: true
      t.references :variant, foreign_key: true
      t.integer :quantity
      t.references :item_type, foreign_key: true
      t.boolean :active

      t.timestamps
    end
  end
end
