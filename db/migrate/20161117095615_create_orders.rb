class CreateOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :orders do |t|
      t.string :number
      t.string :email
      t.string :state
      t.references :user, foreign_key: true
      t.references :coupon, foreign_key: true
      t.boolean :active
      t.boolean :shipped
      t.integer :shipments_counts
      t.datetime :calculated_at
      t.datetime :completed_at
      t.decimal :credited_amount

      t.timestamps
    end
  end
end
