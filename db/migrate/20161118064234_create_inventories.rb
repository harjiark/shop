class CreateInventories < ActiveRecord::Migration[5.0]
  def change
    create_table :inventories do |t|
      t.integer :count_on_hand
      t.integer :count_pending_to_customer
      t.integer :count_pending_from_supplier

      t.timestamps
    end
  end
end
