class CreatePayments < ActiveRecord::Migration[5.0]
  def change
    create_table :payments do |t|
      t.references :invoice, foreign_key: true
      t.integer :confirmation_id
      t.decimal :amount
      t.string :error
      t.string :error_code
      t.string :message
      t.string :action
      t.text :params
      t.boolean :success
      t.boolean :test

      t.timestamps
    end
  end
end
