class CreateInvoices < ActiveRecord::Migration[5.0]
  def change
    create_table :invoices do |t|
      t.references :order
      t.decimal :amount
      t.string :state
      t.boolean :active
      t.decimal :credited_amount

      t.timestamps
    end
  end
end
