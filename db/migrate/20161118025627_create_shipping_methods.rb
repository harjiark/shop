class CreateShippingMethods < ActiveRecord::Migration[5.0]
  def change
    create_table :shipping_methods do |t|
      t.string :name
      t.references :shipping_zone, foreign_key: true

      t.timestamps
    end
  end
end
