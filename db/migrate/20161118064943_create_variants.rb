class CreateVariants < ActiveRecord::Migration[5.0]
  def change
    create_table :variants do |t|
      t.references :product, foreign_key: true
      t.string :sku
      t.string :name
      t.decimal :price
      t.decimal :cost
      t.datetime :deleted_at
      t.boolean :master
      t.references :inventory, foreign_key: true

      t.timestamps
    end
  end
end
