class CreateStates < ActiveRecord::Migration[5.0]
  def change
    create_table :states do |t|
      t.string :name
      t.string :abbreviation
      t.string :described_as
      t.references :country, foreign_key: true
      t.references :shipping_zone, foreign_key: true

      t.timestamps
    end
  end
end
