class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :phone_number
      t.integer :account_id
      t.string :state
      t.string :password_digest
      t.string :access_token
      t.integer :coments_count
      t.datetime :last_access
      t.datetime :expires_at
      t.boolean :is_locked

      t.timestamps
    end
    add_index :users, :email, unique: true
    add_index :users, :phone_number, unique: true
    add_index :users, :access_token, unique: true
  end
end
