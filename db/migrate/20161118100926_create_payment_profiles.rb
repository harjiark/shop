class CreatePaymentProfiles < ActiveRecord::Migration[5.0]
  def change
    create_table :payment_profiles do |t|
      t.references :user, foreign_key: true
      t.references :address, foreign_key: true
      t.integer :payment_cim_id
      t.boolean :default
      t.boolean :active
      t.string :last_digits
      t.string :month
      t.string :year
      t.string :cc_type
      t.string :first_name
      t.string :last_name
      t.string :card_name

      t.timestamps
    end
  end
end
