class CreateProducts < ActiveRecord::Migration[5.0]
  def change

    create_table :products do |t|
      t.string :name
      t.references :product_type, foreign_key:true
      t.text :description
      t.text :product_keywords
      t.references :shipping_category, foreign_key: true
      t.datetime :available_at
      t.datetime :deleted_at
      t.string :meta_keywords
      t.string :meta_description
      t.text :description_markup
      t.boolean :featured
      t.references :brand, foreign_key: true

      t.timestamps
    end
  end
end
