class CreateProductTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :product_types do |t|
      t.string :name
      t.integer :parent_id
      t.boolean :active
      t.integer :rgt
      t.integer :lft

      t.timestamps
    end
  end
end
# 