class CreateCountries < ActiveRecord::Migration[5.0]
  def change
    create_table :countries do |t|
      t.string :name
      t.string :abbreviation
      t.references :shipping_zone, foreign_key: true
      t.boolean :active

      t.timestamps
    end
    add_index :countries, :abbreviation, unique: true
  end
end
