# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)



puts "START SEEDING"

puts "Shipping Zones"
ShippingZone::LOCATIONS.each do |loc|
  ShippingZone.where(name: loc ).first_or_create
end

puts "COUNTRIES"
file_to_load  = Rails.root + 'db/seed/countries.yml'
countries_list   = YAML::load( File.open( file_to_load ) )

# p "countries list:",countries_list

countries_list.each_pair do |key,country|
	s = Country.find_by(abbreviation: country['abbreviation'])
	unless s
		c = Country.create(country) unless s
		c.update_attribute(:active, true) if Country::ACTIVE_COUNTRY_IDS.include?(c.id)
	end
end

puts "States"
file_to_load  = Rails.root + 'db/seed/states.yml'
states_list   = YAML::load( File.open( file_to_load ) )
# p "states_list:",states_list

states_list.each_pair do |key,state|
	s = State.find_by(abbreviation: state['attributes']['abbreviation'], country_id: state['attributes']['country_id'])
	State.create(state['attributes']) unless s
end

puts "ROLES"
roles = Role::ROLES
roles.each do |role|
	Role.find_or_create_by(name: role)
end



      # t.string :type
      # t.string :code
      # t.decimal :amount
      # t.decimal :minimum_value
      # t.integer :percent
      # t.text :description
      # t.datetime :starts_at
      # t.datetime :expires_at

      # t.timestamps



