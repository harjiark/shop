# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161120054423) do

  create_table "address_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "addresses", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "address_type_id"
    t.string   "name"
    t.string   "addresable_type"
    t.integer  "addresable_id"
    t.string   "address1"
    t.string   "address2"
    t.string   "city"
    t.integer  "state_id"
    t.string   "state_name"
    t.string   "zip_code"
    t.string   "alternative_phone"
    t.boolean  "default"
    t.boolean  "billing_default"
    t.boolean  "active"
    t.integer  "country_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["address_type_id"], name: "index_addresses_on_address_type_id", using: :btree
    t.index ["country_id"], name: "index_addresses_on_country_id", using: :btree
    t.index ["state_id"], name: "index_addresses_on_state_id", using: :btree
  end

  create_table "brands", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cart_items", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id"
    t.integer  "cart_id"
    t.integer  "variant_id"
    t.integer  "quantity"
    t.integer  "item_type_id"
    t.boolean  "active"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["cart_id"], name: "index_cart_items_on_cart_id", using: :btree
    t.index ["item_type_id"], name: "index_cart_items_on_item_type_id", using: :btree
    t.index ["user_id"], name: "index_cart_items_on_user_id", using: :btree
    t.index ["variant_id"], name: "index_cart_items_on_variant_id", using: :btree
  end

  create_table "carts", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_carts_on_user_id", using: :btree
  end

  create_table "countries", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "abbreviation"
    t.integer  "shipping_zone_id"
    t.boolean  "active"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["abbreviation"], name: "index_countries_on_abbreviation", unique: true, using: :btree
    t.index ["shipping_zone_id"], name: "index_countries_on_shipping_zone_id", using: :btree
  end

  create_table "coupons", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "type"
    t.string   "code"
    t.decimal  "amount",                      precision: 10
    t.decimal  "minimum_value",               precision: 10
    t.integer  "percent"
    t.text     "description",   limit: 65535
    t.datetime "starts_at"
    t.datetime "expires_at"
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.index ["code"], name: "index_coupons_on_code", unique: true, using: :btree
  end

  create_table "inventories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "count_on_hand"
    t.integer  "count_pending_to_customer"
    t.integer  "count_pending_from_supplier"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "invoices", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "order_id"
    t.decimal  "amount",          precision: 10
    t.string   "state"
    t.boolean  "active"
    t.decimal  "credited_amount", precision: 10
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.index ["order_id"], name: "index_invoices_on_order_id", using: :btree
  end

  create_table "item_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "orders", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "number"
    t.string   "email"
    t.string   "state"
    t.integer  "user_id"
    t.integer  "coupon_id"
    t.boolean  "active"
    t.boolean  "shipped"
    t.integer  "shipments_counts"
    t.datetime "calculated_at"
    t.datetime "completed_at"
    t.decimal  "credited_amount",  precision: 10
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.index ["coupon_id"], name: "index_orders_on_coupon_id", using: :btree
    t.index ["user_id"], name: "index_orders_on_user_id", using: :btree
  end

  create_table "payment_profiles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id"
    t.integer  "address_id"
    t.integer  "payment_cim_id"
    t.boolean  "default"
    t.boolean  "active"
    t.string   "last_digits"
    t.string   "month"
    t.string   "year"
    t.string   "cc_type"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "card_name"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["address_id"], name: "index_payment_profiles_on_address_id", using: :btree
    t.index ["user_id"], name: "index_payment_profiles_on_user_id", using: :btree
  end

  create_table "payments", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "invoice_id"
    t.integer  "confirmation_id"
    t.decimal  "amount",                        precision: 10
    t.string   "error"
    t.string   "error_code"
    t.string   "message"
    t.string   "action"
    t.text     "params",          limit: 65535
    t.boolean  "success"
    t.boolean  "test"
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.index ["invoice_id"], name: "index_payments_on_invoice_id", using: :btree
  end

  create_table "product_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.integer  "parent_id"
    t.boolean  "active"
    t.integer  "rgt"
    t.integer  "lft"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "products", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.integer  "product_type_id"
    t.text     "description",          limit: 65535
    t.text     "product_keywords",     limit: 65535
    t.integer  "shipping_category_id"
    t.datetime "available_at"
    t.datetime "deleted_at"
    t.string   "meta_keywords"
    t.string   "meta_description"
    t.text     "description_markup",   limit: 65535
    t.boolean  "featured"
    t.integer  "brand_id"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.index ["brand_id"], name: "index_products_on_brand_id", using: :btree
    t.index ["product_type_id"], name: "index_products_on_product_type_id", using: :btree
    t.index ["shipping_category_id"], name: "index_products_on_shipping_category_id", using: :btree
  end

  create_table "roles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_roles_on_name", unique: true, using: :btree
  end

  create_table "shipping_categories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "shipping_methods", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.integer  "shipping_zone_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["shipping_zone_id"], name: "index_shipping_methods_on_shipping_zone_id", using: :btree
  end

  create_table "shipping_zones", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "states", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "abbreviation"
    t.string   "described_as"
    t.integer  "country_id"
    t.integer  "shipping_zone_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["country_id"], name: "index_states_on_country_id", using: :btree
    t.index ["shipping_zone_id"], name: "index_states_on_shipping_zone_id", using: :btree
  end

  create_table "suppliers", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_roles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id"
    t.integer  "role_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["role_id"], name: "index_user_roles_on_role_id", using: :btree
    t.index ["user_id"], name: "index_user_roles_on_user_id", using: :btree
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone_number"
    t.integer  "account_id"
    t.string   "state"
    t.string   "password_digest"
    t.string   "access_token"
    t.integer  "coments_count"
    t.datetime "last_access"
    t.datetime "expires_at"
    t.boolean  "is_locked"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["access_token"], name: "index_users_on_access_token", unique: true, using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["phone_number"], name: "index_users_on_phone_number", unique: true, using: :btree
  end

  create_table "variants", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "product_id"
    t.string   "sku"
    t.string   "name"
    t.decimal  "price",        precision: 10
    t.decimal  "cost",         precision: 10
    t.datetime "deleted_at"
    t.boolean  "master"
    t.integer  "inventory_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.index ["inventory_id"], name: "index_variants_on_inventory_id", using: :btree
    t.index ["product_id"], name: "index_variants_on_product_id", using: :btree
  end

  add_foreign_key "addresses", "address_types"
  add_foreign_key "addresses", "countries"
  add_foreign_key "addresses", "states"
  add_foreign_key "cart_items", "carts"
  add_foreign_key "cart_items", "item_types"
  add_foreign_key "cart_items", "users"
  add_foreign_key "cart_items", "variants"
  add_foreign_key "carts", "users"
  add_foreign_key "countries", "shipping_zones"
  add_foreign_key "orders", "coupons"
  add_foreign_key "orders", "users"
  add_foreign_key "payment_profiles", "addresses"
  add_foreign_key "payment_profiles", "users"
  add_foreign_key "payments", "invoices"
  add_foreign_key "products", "brands"
  add_foreign_key "products", "product_types"
  add_foreign_key "products", "shipping_categories"
  add_foreign_key "shipping_methods", "shipping_zones"
  add_foreign_key "states", "countries"
  add_foreign_key "states", "shipping_zones"
  add_foreign_key "user_roles", "roles"
  add_foreign_key "user_roles", "users"
  add_foreign_key "variants", "inventories"
  add_foreign_key "variants", "products"
end
