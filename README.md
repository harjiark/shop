
Description:
  This is a simple e-commerce application


Environment details:

  . ruby 2.3.1
  . rails 5.0.0
  . mysql 5.6.31

Configuration:
See database.yml file


How to start:
In your root project
  1. gem install bundler
  2. bundle install (fetch dependencies)
  3. rake db:drop db:create db:migrate db:seed db:seed_fake (migration and seed database)
  4. rails s (star the server)

Message example:

1. Register
   url = http://localhost:3001/users
   method = push
   headers :

    Content-Type = application/json

   request :
   {
  "user":{
    "name":"harji5",
    "email":"harji28gmail.com",
    "password":"harji",
    "password_confirmation":"harji",
    "phone_number":"587"

        }
    }

    response :
    {"id":4,"name":"harji5","phone_number":"587","access_token":"18697edcd6664014745aa5d7aa59014c"}

 2. authentication :

   http method = POST
   url : /auth
   Request:
   {
      "phone_number" : "089191991",
      "password" : "harji"
   }

   response:

   {
    "id": 1,

    "activity_type": {
      "id": 2,
      "name": "online",
      "description": "user online"
    },
    "user": {
      "id": 4,
      "name": "harji4",
      "phone_number": "089191991",
          "user_type": {
            "id": 2,
            "name": "passenger",
         "description": "user passenger"
        }
         }
      }

      use header value of X-ACCESS-TOKEN for as token value of next operation

  3. show products list
     Content-Type = application/json
     http method = GET
     url : http://localhost:3001/products
     additional heander:
     X-ACCESS-TOKEN = e432cdcb9b0411e16a002117f75214c2

     response :

     [
   {
    "id": 1,
    "name": "Cute Polo",
    "description": "<p>This shirt is made for developers.</p> <hr /> <p>You will love it.</p>",
    "product_type": {
      "id": 2,
      "name": "shirts"
    },
    "product_keywords": "[\"polo\", \"shirt\"]",
    "meta_keywords": "polo shirt",
    "meta_description": "polo shirt for your wearing pleasure",
    "description_markup": "This shirt is made for developers.\n\n      ----------      \n\n You will love it."
   },
   {
    "id": 2,
    "name": "Cute Pants",
    "description": "<p>These pants are Great.  Soft as a baby's bottom.</p> <hr /> <p>You will love them.</p>",
    "product_type": {
      "id": 3,
      "name": "pants"
    },
    "product_keywords": "[\"polo\", \"shirt\"]",
    "meta_keywords": "polo pants",
    "meta_description": "polo pants for your wearing pleasure",
    "description_markup": "These pants are Great.  Soft as a baby's bottom.\n\n      ----------      \n\n You will love them."
   }
  ]


4. Show users list (admin only)

     Content-Type = application/json
     http method = GET
     url : http://localhost:3001/products
     additional heander:
     X-ACCESS-TOKEN = e432cdcb9b0411e16a002117f75214c2

     response:
     [
  {
    "id": 1,
    "name": "Admin",
    "phone_number": "98987988787"
  },
  {
    "id": 2,
    "name": "harji",
    "phone_number": "8797982343098709"
  },
  {
    "id": 3,
    "name": "harji2",
    "phone_number": "8782343098709"
  }
]

5. Show coupon list (amin only)

   Content-Type = application/json
     http method = GET
     url : http://localhost:3001/products
     additional heander:
     X-ACCESS-TOKEN = e432cdcb9b0411e16a002117f75214c2

   response:
   [
  {
    "id": 1,
    "type": "CouponValue",
    "code": "1",
    "amount": 10,
    "minimum_value": 30,
    "percent": 30,
    "description": "this is coupon description",
    "starts_at": "2016-11-11T00:00:00.000Z",
    "expires_at": "2016-12-11T00:00:00.000Z",
    "orders": []
  },
  {
    "id": 2,
    "type": "CouponFirstPurchasePercent",
    "code": "2",
    "amount": 10,
    "minimum_value": 30,
    "percent": 30,
    "description": "this is coupon description",
    "starts_at": "2016-11-11T00:00:00.000Z",
    "expires_at": "2016-12-11T00:00:00.000Z",
    "orders": []
  }
]

for admin fake
user     : admin@notarealemail.com
password : admin
