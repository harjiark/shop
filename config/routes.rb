Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'admin/overviews'       => 'admin/overviews#index'


  post 'users' => 'users#create'
  put  'users' => 'users#update'

  post 'auth' => 'authentications#create'
  delete 'auth' => 'authentications#destroy'

  resources :products,      only: [:index, :show, :create]

  namespace :shopping do
    resources  :addresses do
      member do
        put :select_address
      end
    end

    resources  :billing_addresses do
      member do
        put :select_address
      end
    end

    resources  :cart_items do
      member do
        put :move_to
      end
    end

    resource  :coupon, only: [:show, :create]

    resources  :orders do
      member do
        get :checkout
        get :confirmation
      end
    end
    resources  :shipping_methods
  end

  namespace :admin do
    resources :users
    namespace :generic do
      resources :coupons
      # resources :deals
      # resources :sales
    end
  end



end
