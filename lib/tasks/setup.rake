namespace :db do
    task :seed_fake => :environment do

      def User.digest(string)
  cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
  BCrypt::Engine.cost
  BCrypt::Password.create(string, cost: cost)
end

def args

  @args ||= {
    :name => 'Admin',
    :email => 'admin@notarealemail.com',
    :phone_number => '98987988787',
    :password_digest => User.digest('admin')
  }
end


puts "admin"

admin = User.find_by_email(args[:email])
unless admin
    p "inside admin unless"
    admin = User.new(args)
    admin.role_ids = Role.all.map{|r| r.id }
    admin.save
end

def Coupon.coupons

  coupon1 = {

    :code => "1",
    :type => Coupon::COUPON_TYPES[1],
    :amount => "10",
    :minimum_value => "30",
    :percent => "30",
    :description => "this is coupon description",
    :starts_at => "11-11-2016",
    :expires_at  => "11-12-2016"

  }


  coupon2 = {

    :code => "2",
    :type => Coupon::COUPON_TYPES[2],
    :amount => "10",
    :minimum_value => "30",
    :percent => "30",
    :description => "this is coupon description",
    :starts_at => "11-11-2016",
    :expires_at  => "11-12-2016"

  }
  coupons = Array.new
  coupons << coupon1
  coupons << coupon2
end

puts "coupons" , Coupon.coupons.length


coupons = Coupon.coupons

coupons.each do |coupon|
  # Coupon.find_or_create_by(code: coupon[:code])
  coupon_ob = Coupon.find_by_code(coupon[:code])
  unless coupon_ob
    coupon_ob = Coupon.new(coupon)
    coupon_ob.save
  end
end


      if defined?(Product.solr_search)
        begin
          Rake::Task["sunspot:solr:start"].invoke
        rescue Exception => e
          puts e
        end
      end

      file_to_load        = Rails.root + "db/seed/config_admin.yml"
      config_information  = YAML::load( File.open( file_to_load ) )
      unless Brand.first
        if Role.first
          config_information.each do |table|
          #puts table[0].singularize.camelcase
          tables_model = table[0].singularize.camelcase.constantize
          table[1].each_pair do |num, attribute_hash|
            puts [num, tables_model.to_s].join(' | ')
            tables_model.create( attribute_hash['attributes'])
          end
        end
      else
        puts 'PLEASE RUN "rake db:seed" before running "rake db:seed_fake"'
      end
    else
      puts 'IT APPEAR YOU ALREADY HAVE DATA. PLEASE truncate your DB or take a look because it might be ready to go!!'
    end

    Product.update_all(deleted_at: nil)
  end
end
