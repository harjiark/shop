 # t.string :type
 #      t.string :code
 #      t.decimal :amount
 #      t.decimal :minimum_value
 #      t.integer :percent
 #      t.text :description
 #      t.datetime :starts_at
 #      t.datetime :expires_at

 class CouponSerializer < ActiveModel::Serializer
 	attributes :id, :type , :code, :amount, :minimum_value, :percent, :description, :starts_at, :expires_at
 	has_many :orders
  # has_one :user, embed: :id
end
