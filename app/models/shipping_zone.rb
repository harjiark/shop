class ShippingZone < ApplicationRecord
	has_many :shipping_methods
  ## if your model requires more shipping zones create a join table and delete state.shipping_zone_id
  #has_many :state_shipping_zones
  has_many :states#, :through => :state_shipping_zones

  JABOTABEK         = 'JABOTABEK'
  JABAR = 'JABAR'
  JATENG        = 'JATENG'
  JATIM = 'JATIM'
  #OTHER_STATE   = 'Other States'

  LOCATIONS     = [JABOTABEK, JABAR, JATENG, JATIM]#, OTHER_STATE]

  validates :name, presence: true, length: { maximum: 255 }

  accepts_nested_attributes_for :states

end
