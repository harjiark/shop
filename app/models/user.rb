class User < ApplicationRecord

  has_secure_password

  validates :phone_number , presence: true, uniqueness: true
  validates :email , presence: true, uniqueness: true
  validates :name , presence: true


  has_many    :user_roles,                dependent: :destroy
  has_many    :roles,                     through: :user_roles


  before_create :generate_access_token, :set_expiry_date

  def find_api_key
    self
  end

  def as_json(options={})
    {
     :id => id,
     :name => name,
     :email => email
   }
 end

 def set_expiry_date
  self.expires_at = 1.day.from_now
end

def generate_access_token
  begin
    self.access_token = SecureRandom.hex
  end while self.class.exists?(access_token: access_token)
end

def is_expired?
  return self.expires_at <= Time.now ? true : false
end


def self.admin_grid(params = {})
  includes(:roles).name_filter(params[:name]).email_filter(params[:email])
end




def active?
  !['canceled', 'inactive'].any? {|s| self.state == s }
end

  	# returns true or false if the user has a role or not
  	#
  	# @param [String] role name the user should have
  	# @return [ Boolean ]
  	def role?(role_name)
  		roles.any? {|r| r.name == role_name.to_s}
  	end

  	# returns true or false if the user is an admin or not
  	#
  	# @param [none]
  	# @return [ Boolean ]
  	def admin
  		p "role:",role?(:administrator)
      role?(:administrator) || role?(:super_administrator)
  	end

  	# returns true or false if the user is a super admin or not
  	# FYI your IT staff might be an admin but your CTO and IT director is a super admin
  	#
  	# @param [none]
  	# @return [ Boolean ]
  	def super_admin?
  		role?(:super_administrator)
  	end

    private

    def self.name_filter(name)
      name.present? ? where("users.name LIKE ?", "#{name}%") : all
    end


    def self.email_filter(email)
      email.present? ? where("users.email LIKE ?", "#{email}%") : all
    end



    def sanitize_data
    self.email      = self.email.strip.downcase   unless email.blank?
    self.name = self.name.strip.capitalize  unless name.nil?


    ## CHANGE THIS IF YOU HAVE DIFFERENT ACCOUNT TYPES
    # self.account = Account.first unless account_id
     end
  end
