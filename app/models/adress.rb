class Adress < ApplicationRecord
  belongs_to :adress_type
  belongs_to :state
  belongs_to :country
end
