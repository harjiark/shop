class CartItem < ApplicationRecord
  belongs_to :user
  belongs_to :cart
  belongs_to :variant
  belongs_to :item_type
end
