class Variant < ApplicationRecord
  belongs_to :product
  belongs_to :inventory

    # {
    #     "id": 1,
    #     "product_id": 1,
    #     "sku": "0000-0000-000001",
    #     "name": null,
    #     "price": 30,
    #     "cost": 10,
    #     "deleted_at": null,
    #     "master": null,
    #     "inventory_id": 1,
    #     "created_at": "2016-11-20T05:48:21.000Z",
    #     "updated_at": "2016-11-20T05:48:21.000Z"
    #   },

  def as_json(options={})
    {
      :id => id,
      :name => name,
      :cost => cost,
      :price => price
    }
  end
end
