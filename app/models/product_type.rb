class ProductType < ApplicationRecord

    def as_json(options={})
    {
      :id => id,
      :name => name

    }
  end
end
