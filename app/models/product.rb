class Product < ApplicationRecord
  belongs_to :shipping_category
  belongs_to :brand

  belongs_to :brand
  belongs_to :product_type
  belongs_to :prototype
  belongs_to :shipping_category

  has_many :product_properties
  has_many :properties,         through: :product_properties

  has_many :variants

  # Price of cheapest variant
  #
  # @param [none] the size of the image expected back
  # @return [Decimal] price

  # attributes :id, :name, :description, :product_type ,:product_keywords, :meta_keywords, :meta_description, :description_markup
  def as_json(options={})
    {
      :id => id,
      :name => name,
      :description => description,
      :product_type => product_type
      # :variants =>variants
    }
  end

  def price
    active_variants.present? ? price_range.first : raise( VariantRequiredError )
  end

  # in the admin form this is the method called when the form is submitted, The method sets
  # the product_keywords attribute to an array of these values
  #
  # @param [String] value for set_keywords in a products form
  # @return [none]
  def set_keywords=(value)
    self.product_keywords = value ? value.split(',').map{|w| w.strip} : []
  end

  # method used by forms to set the array of keywords separated by a comma
  #
  # @param [none]
  # @return [String] product_keywords separated by comma
  def set_keywords
    product_keywords ? product_keywords.join(', ') : ''
  end

  # range of the product prices (Just teh low and high price) as an array
  #
  # @param [none]
  # @return [Array] [Low price, High price]
  def price_range
    return @price_range if @price_range
    return @price_range = ['N/A', 'N/A'] if active_variants.empty?
    @price_range = active_variants.minmax {|a,b| a.price <=> b.price }.map(&:price)
  end

  # Answers if the product has a price range or just one price.
  #   if there is more than one price returns true
  #
  # @param [none]
  # @return [Boolean] true == there is more than one price
  def price_range?
    !(price_range.first == price_range.last)
  end

  def self.active
    where("products.deleted_at IS NULL OR products.deleted_at > ?", Time.zone.now)
    #  Add this line if you want the available_at to function
    #where("products.available_at IS NULL OR products.available_at >= ?", Time.zone.now)
  end

  def active(at = Time.zone.now)
    deleted_at.nil? || deleted_at > (at + 1.second)
  end

  def active?(at = Time.zone.now)
    active(at)
  end

  def activate!
    self.deleted_at = nil
    save
  end

  def available?
    has_shipping_method? && has_active_variants?
  end

  def sold_out?
    active_variants.all?(&:sold_out?)
  end

  def stock_status
    if low_stock?
      "low_stock"
    elsif sold_out?
      "sold_out"
    else
      "available"
    end
  end

  def low_stock?
    active_variants.any?(&:low_stock?)
  end

  # returns the brand's name or a blank string
  #  ex: obj.brand_name => 'Nike'
  #
  # @param [none]
  # @return [String]
  def brand_name
    brand_id ? brand.name : ''
  end

  def has_shipping_method?
    shipping_category.shipping_rates.exists?
  end

  private

    def has_active_variants?
      active_variants.any?{|v| v.is_available? }
    end

    def create_content
      self.description = BlueCloth.new(self.description_markup).to_html unless self.description_markup.blank?
    end

    def not_active_on_create!
      self.deleted_at ||= Time.zone.now
    end
end
